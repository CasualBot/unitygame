﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]

public class PlayerPhysics : MonoBehaviour {

	public LayerMask cMask;
	private BoxCollider box;
	private Vector3 size;
	private Vector3 center;
	private float skin = .005f;

	private Vector3 originalSize;
	private Vector3 originalCenter;
	private float colliderScale;

	private int collisionDivisionX = 3;
	private int collisionDivisionY = 10;

	[HideInInspector]
	public bool grounded;
	
	[HideInInspector]
	public bool movementStopped;

	Ray ray;
	RaycastHit hit;

	void Start() {
		box = GetComponent<BoxCollider> ();
		colliderScale = transform.localScale.x;

		originalSize = box.size;
		originalCenter = box.center;
		SetCollider (originalSize, originalCenter);
	}

	public void Move(Vector2 moveAmount) {

		float deltaY = moveAmount.y;
		float deltaX = moveAmount.x;
		Vector2 p = transform.position;
		grounded = false;

		for (int i = 0; i < collisionDivisionX; i++) {
			float dir = Mathf.Sign (deltaY);
			float x = (p.x + center.x - size.x / 2) + size.x/(collisionDivisionX-1) * i;
			float y = p.y + center.y + size.y / 2 * dir;

			ray = new Ray (new Vector2 (x, y), new Vector2 (0, dir));
			Debug.DrawRay(ray.origin,ray.direction);
			if (Physics.Raycast (ray, out hit, Mathf.Abs (deltaY) + skin, cMask)) {

				float dst = Vector3.Distance (ray.origin, hit.point);

				if (dst > skin) { 
					deltaY = dst * dir - skin * dir;
				} else {
					deltaY = 0;
				}

				grounded = true;
				break;
			}
		}

		movementStopped = false;
		for (int i = 0; i < collisionDivisionY; i++) {
			float dir = Mathf.Sign (deltaX);
			float x = p.x + center.x + size.x/2 * dir;
			float y = p.y + center.y - size.y/2 + size.y/(collisionDivisionY-1) * i;
			
			ray = new Ray(new Vector2(x,y), new Vector2(dir, 0));
			Debug.DrawRay(ray.origin, ray.direction);
			
			if (Physics.Raycast(ray, out hit, Mathf.Abs (deltaX) + skin, cMask)) {
				
				float dst = Vector3.Distance(ray.origin, hit.point);
				
				if (dst > skin ) { 
					deltaX = dst * dir - skin * dir;
				} else {
					deltaX = 0;
				}

				movementStopped = true;
				break;

			}
		}
		if (!grounded && !movementStopped) {
			Vector3 playerDir = new Vector3 (deltaX, deltaY, 0);
			Vector3 o = new Vector3(p.x + center.x + size.x/2 * Mathf.Sign (deltaX), p.y + center.y + size.y / 2 * Mathf.Sign (deltaY), 0);
			ray = new Ray (o, playerDir.normalized);
			
			if (Physics.Raycast (ray, Mathf.Sqrt (deltaX * deltaX + deltaY * deltaY), cMask)) {
				grounded = true;
				deltaY = 0;
			}
		}


		Vector2 finalTransform = new Vector2(deltaX, deltaY);

		transform.Translate (finalTransform, Space.World);

	}

	public void SetCollider(Vector3 size2, Vector3 center2) {
		box.size = size;
		box.center = center;

		size = size2 * colliderScale;
		center = center2 * colliderScale;
		
	}
}
