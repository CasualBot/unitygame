﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerPhysics))]

public class PlayerController : MonoBehaviour {

	public float gravity = 20;
	public float walkSpeed = 8;
	public float runSpeed = 12;
	public float accel = 12;
	public float jumpHeight = 12;

	private float animationSpeed;
	private float currentSpeed;
	private float targetSpeed;
	private Vector2 amountToMove;
	
	private PlayerPhysics playerPhysics;
	private Animator animator;
	
	// Use this for initialization
	void Start () {
		playerPhysics = GetComponent<PlayerPhysics> ();
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (playerPhysics.movementStopped) {
			targetSpeed = 0;
			currentSpeed = 0;
		}

		if (playerPhysics.grounded) {
			
			amountToMove.y = 0;
			
			if (Input.GetButtonUp ("Jump")) {
				amountToMove.y = jumpHeight;
			}
		}

		animationSpeed = IncrementTowards (animationSpeed, Mathf.Abs (targetSpeed), accel);
		animator.SetFloat ("Speed", animationSpeed);

		float speed = (Input.GetButton ("Run") ? runSpeed : walkSpeed);
		targetSpeed = Input.GetAxisRaw ("Horizontal") * speed;
		currentSpeed = IncrementTowards (currentSpeed, targetSpeed, accel);

		amountToMove.x = currentSpeed;
		amountToMove.y -= gravity * Time.deltaTime;
		playerPhysics.Move (amountToMove * Time.deltaTime);

		float movDir = Input.GetAxisRaw ("Horizontal");
		if (movDir != 0) {
			transform.eulerAngles = (movDir > 0) ? Vector3.up * 180 : Vector3.zero;
		}

	}
	
	private float IncrementTowards(float n, float target, float speed) {
		if (n == target) {
			return n;
		} else {
			float dir = Mathf.Sign(target - n);
			n += speed * Time.deltaTime *dir;
			return (dir == Mathf.Sign (target-n)) ? n : target;
		}
	}
}
